import RSA
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.filedialog as filedialog

root = tk.Tk()
root.configure(background='powder blue')

style = ttk.Style()
style.theme_use('clam')

#North West East South sticky variables
N = tk.N
W = tk.W
E = tk.E
S = tk.S

def publicKeyFileName():
    fileNamePub= filedialog.askopenfilename(filetypes=[("Key Files", "*.key", "TEXT"), ("All Files", "*")]) 
    print(fileNamePub)
    fileEntryPubKey.delete(0, tk.END)
    fileEntryPubKey.insert(0, fileNamePub)
    return fileNamePub

def privateKeyFileName():
    fileNamePriv= filedialog.askopenfilename(filetypes=[("Key Files", "*.key", "TEXT"), ("All Files", "*")]) 
    print(fileNamePriv)
    fileEntryPrivKey.delete(0, tk.END)
    fileEntryPrivKey.insert(0, fileNamePriv)
    return fileNamePriv


def encryptMessage():
    pubKeyFileLocation = fileEntryPubKey.get()
    dataUnEncrypted = encTextIn.get("1.0",tk.END)

    dataEncrypted = RSA.encrypt(dataUnEncrypted.encode('utf-8'), pubKeyFileLocation)
    encTextOut.delete("1.0", tk.END)
    encTextOut.insert("1.0", dataEncrypted)

def decryptMessage():
    privKeyFileLocation = fileEntryPrivKey.get()
    dataEncrypted = decTextIn.get("1.0",tk.END)

    dataDecrypted = RSA.decrypt(dataEncrypted, privKeyFileLocation)
    decTextOut.delete("1.0", tk.END)
    decTextOut.insert("1.0", dataDecrypted)

# C0/C1
titleLabel = tk.Label(root, text="RSA Encryption", font=("Helvetica", 16), background='powder blue')
titleLabel.grid(row=0, column=0, columnspan=2)

# File entry and browse button
fileEntryPubKey = tk.Entry(root, text="Public key") #this one
fileEntryPubKey.grid(row=1, column=0, sticky=W+E+N+S) 

fileButton = tk.Button(root, text="Browse", command=lambda:publicKeyFileName())
fileButton.grid(row=1, column=1, sticky=W+E+N+S)


encLabelInput = tk.Label(root, text="Input", font=("Helvetica", 12), background='powder blue')
encLabelInput.grid(row=2, column=0, columnspan=2)

encTextIn = tk.Text(root, height=8, width=50)
encTextIn.grid(row=3, column=0, columnspan=2)

encLabelOutput = tk.Label(root, text="Output", font=("Helvetica", 12), background='powder blue')
encLabelOutput.grid(row=4, column=0, columnspan=2)

encTextOut = tk.Text(root, height=8, width=50)
encTextOut.grid(row=5, column=0, columnspan=2)

encButton = tk.Button(root, text="Encrypt", command=encryptMessage)
encButton.grid(row=6, column=0, columnspan=2, sticky=W+E+N+S)

# C2/C3
titleLabel = tk.Label(root, text="RSA Decryption", font=("Helvetica", 16), background='powder blue')
titleLabel.grid(row=0, column=2, columnspan=2)

# File entry and browse button
fileEntryPrivKey = tk.Entry(root, text="Private key") #this one
fileEntryPrivKey.grid(row=1, column=2, sticky=W+E+N+S)

fileButtonpriv = tk.Button(root, text="Browse", command=lambda:privateKeyFileName())
fileButtonpriv.grid(row=1, column=3, sticky=W+E+N+S)


encLabelInput = tk.Label(root, text="Input", font=("Helvetica", 12), background='powder blue')
encLabelInput.grid(row=2, column=2, columnspan=2)


decTextIn = tk.Text(root, height=8, width=50)
decTextIn.grid(row=3, column=2, columnspan=2)

encLabelOutput = tk.Label(root, text="Output", font=("Helvetica", 12), background='powder blue')
encLabelOutput.grid(row=4, column=2, columnspan=2)

decTextOut = tk.Text(root, height=8, width=50)
decTextOut.grid(row=5, column=2, columnspan=2)

decButton = tk.Button(root, text="Decrypt", command=decryptMessage)
decButton.grid(row=6, column=2, columnspan=2, sticky=W+E+N+S)

root.mainloop()