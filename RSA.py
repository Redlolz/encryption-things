from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto import Random

import qrcode
# from qrcode.image.pure import PymagingImage

import base64

def encrypt(data, pubKeyFile):
    key = RSA.importKey(open(pubKeyFile).read())
    cipher = PKCS1_OAEP.new(key)
    cipherText = cipher.encrypt(data)
    return cipherText

def decrypt(data, privKeyFile):
    key = RSA.importKey(open(privKeyFile).read())
    cipher = PKCS1_OAEP.new(key)
    message = cipher.decrypt(data)
    return message

def keyGen(publicKeyFileName, privateKeyFileName, password):
    randomGen = Random.new().read
    key = RSA.generate(2048, randomGen)

    pubKey = key.publickey().exportKey()
    exportKeys = key.exportKey(format='PEM', passphrase=password, pkcs=1)

    pubKeyFile = open(publicKeyFileName, 'wb')
    privKeyFile = open(privateKeyFileName, 'wb')

    pubKeyFile.write(pubKey)
    privKeyFile.write(exportKeys)

    print(exportKeys.decode())
    print('\n')
    print(pubKey.decode())

def QRGeneratorPNG(data, ver, boxSize, borderSize):
    qr = qrcode.QRCode(
        version=ver,
        error_correction=qrcode.constants.ERROR_CORRECT_M,
        box_size=boxSize,
        border=borderSize,
    )
    qr.add_data(data)
    # return qrcode.make(fit=True, image_factory=PymagingImage)
    # img = qr.make_image(fill_color="black", back_color="white")
    # qr.print_ascii()
    # return base64.b64encode(img.tobytes()).decode('utf-8')

def QRGeneratorTerm(data, ver, boxSize, borderSize):
    qr = qrcode.QRCode(
        version=ver,
        error_correction=qrcode.constants.ERROR_CORRECT_M,
        box_size=boxSize,
        border=borderSize,
    )
    qr.add_data(data)
    qr.make(fit=True)
    qr.print_ascii()

def publicKeyQRCode(publicKeyFileName):
    with open(publicKeyFileName, 'r') as myfile:
        # publicKey = myfile.read().replace('\n', '')
        publicKey = myfile.read()
    QRGeneratorTerm(publicKey, 1, 2, 2)

def testFunctions():
    keyGen('publicTest.key', 'privateTest.key', None)
    
    publicKeyQRCode('publicTest.key')
    
    message = b'this is a message that is going to be encrypted and decrypted'
    messageFile = open('message.txt', 'wb')
    messageFile.write(message)
    print(message)
    
    encryptedMessage = encrypt(message, 'public.key')
    encryptedMessageFile = open('encryptedMessage.txt', 'wb')
    encryptedMessageFile.write(encryptedMessage)
    print(encryptedMessage)
    
    decryptedMessage = decrypt(encryptedMessage, 'private.key')
    print('decrypted: ' + decryptedMessage.decode('utf-8'))
